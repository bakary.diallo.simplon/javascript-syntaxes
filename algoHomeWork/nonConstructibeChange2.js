
function newOne(coins) {
    let sortCoin = coins.sort((a, b) => a - b)
    let currentChange = 0

    for (let i = 0; i < sortCoin.length; i++) {
        if (sortCoin[i] > currentChange + 1) return currentChange + 1
        currentChange += sortCoin[i]
    }
    return currentChange + 1
}
const coins = [5, 7, 1, 1, 2, 3, 22] // 20

console.log(newOne(coins))
