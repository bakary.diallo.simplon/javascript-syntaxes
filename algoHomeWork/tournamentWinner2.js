const HOME_TEAM_WON = 1

function updateScore(team, points, scores) {
    if (!(team in scores)) scores[team] = 0
    scores[team] += points;
}

function tournamentWinner(competitions, results) {

    let currentBestTeam = ''
    const scores = {[currentBestTeam]: 0}

    for (let idx = 0; idx < competitions.length; idx++) {
        const result = results[idx]
        const [homeTeam, awayTeam] = competitions[idx]
        const winningTeam = result === HOME_TEAM_WON ? homeTeam : awayTeam

        updateScore(winningTeam, 3, scores)

        if (scores[winningTeam] > scores[currentBestTeam]) {
            currentBestTeam = winningTeam
        }
    }
    return currentBestTeam
}

let competitions = [
    ["HTML", "C#"],
    ["C#", "Python"],
    ["Python", "HTML"]
]
let results = [0, 0, 1]

console.log(tournamentWinner(competitions, results))
